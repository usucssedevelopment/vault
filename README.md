# vault

This repository contains variations of a simple class library for confidential-information vault for the purpose of illustrating good and bad abstraction, modularity, and encapsulation.  Below is a list of variation sub-directories.

* basic/
  * good modularity/
    * good localization of design decision, coupling, cohence, modular reasoning, and congruency
  * poor congruency
    * overstatements
	* understandments
 
We still want to create the following

* basic/
  * poor localization/
  * poor coupling/
  * poor cohension/
  * poor modular reasoning/
